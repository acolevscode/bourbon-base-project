# StyleSheets

Stylesheets are built using compass.

## Build and Distribute 

```sh
$ grunt styles
```

## Commands

Build the css files for production.

```sh
$ grunt compass:dist
```

Build the css files for development.

```sh
$ grunt compass:dev
```

Copy any static css files from the source folder to the tmp folder

```sh
$ grunt copy:tmpcss
```

Copy all css files to the site and patternlab

```sh
$ grunt copy:css
```