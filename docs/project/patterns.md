# Pattern Libraries

SASS documentation is generated using SASSDoc

```sh
$ grunt sassdoc
```

The Pattern Librari is generated using PatternLab

```sh
$ grunt exec:patternlab
```
