# HTML Files

HTML files go through ProcessHTML and HTMLmin during processing.

## Commands

Build and process the html files for the production environment.

```sh
$ grunt html
```

Build and process the html files for the dev environment

```sh
$ grunt html.dev
```

## ProcessHTML

The ProcessHTML task allows us to add control blocks directly inside the html files. See https://www.npmjs.com/package/grunt-processhtml form more information.

The dev task preps the html files the dev environment.

```sh
$ grunt processhtml:dev
```

The dist task preps the html files for the produciton environment.

```sh
$ grunt processhtml:dist
```

## HTMLmin

HTMLmin minifies the html code

```sh
$ grunt htmlmin
```
