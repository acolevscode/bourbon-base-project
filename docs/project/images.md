# Images

.png, .jpg, .gif, .svg are optimized through Imagemin and saved to the tmp directory. Once they are optimized a png copy is made of the svg images as a fallback. Then the finialized images are copied from the tmp directory to the /httpdocs/site, and the /httpdocs/patternlab/source directory.

## Command

Optimize the images

```sh
$ grunt imagemin
```

Generate PNG fallbacks

```sh
$ grunt svg2png
```

Copy the files to their destinations

```sh
$ grunt copy:images
```