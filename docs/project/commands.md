
## Build & Optimize

```sh
$ grunt
```

Build and optimize the current project, ready for deployment.
This includes linting as well as image, script, stylesheet and HTML optimization and minification.

There are many commands available to help you build and test sites. Here are a few highlights to get started with.

## Watch For Changes & Automatically Rebuild site assets as they change

```sh
$ grunt dev
```

## Copy generic files, i.e. non .js, .html, .css, .scss, .gif, etc

```sh
$ grunt copy:generic
```

## Build the JavaScript files

This will lint, compile, run the unit tests, and generate the Plato complexity reports for the application JavaScript files.

```sh
$ grunt js
```

### Copy the static libraries over

```sh
$ grunt copy:jsStatic
```

### Lint the JavaScript files 

Linting is done through JSHint, and JSCS.

```sh
$ grunt jshint
```

```sh
$ grunt jscs
```

### Concatenate and Compress the JavaScript files

The uglify script generates 3 files.
- base.min.js
  - This file includes all of the required libraries plus the application scripts in one file. This is the production javascript file.
- script.min.js
  - This only contains the application javascript files. This allows us to seperate the library files from the custom code for testing and development.
- script.min.spec.js
  - These are our javascript unit tests.

```sh
$ grunt uglify
```

### Run JavaScript Unit tests

```sh
$ grunt jasmine:dev
```

### Analyize the JavaScript files

```sh
$ grunt plato
```

## Process application images

All images will be minfied, and svg iamges will have a matching png generated.

```sh
$ grunt images
```

## Process html files

HTML files will be processed and minified.

### Generate Development html files

```sh
$ grunt html.dev
```

### Generate Production html files

```sh
$ grunt html
```

## Process the StyleSheets

Copy all css files into the dist folder, and process all sass files appropriately.

```sh
$ grunt styles
```

### Copy all of the css files over

```sh
$ grunt copy:css
```

### Process the SASS files

```sh
$ grunt sass:dist
```
