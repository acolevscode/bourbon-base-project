// describe('array.find polyfill', function() {
//   it('should be testing', function() {
//     expect(true).toEqual(true);
//   });

//   it('should be defined', function() {
//     expect(Array.prototype.find).toBeDefined();
//   });

//   it('should throw an error if the array is not defined', function() {
//     expect(function() { 
//       Array.prototype.find.call(null, function() { return false; });
//     }).toThrow(new TypeError('Array.prototype.find called on null or undefined'));
//   });

//   it('should throw an error if the predicate is not defined', function() {
//     expect(function() { 
//       [1, 2, 4, 5, 6].find();
//     }).toThrow(new TypeError('predicate must be a function'));
//   });

// });

QUnit.test( 'array.find', function( assert ) {
  assert.ok( 1 == "1", "Passed!" );

  assert.ok(Array.prototype.find !== undefined, 'find is undefined');

  var arr = [1,2,3,4,5,6,7,9,0];

  var findNumber = function(value, index, list) {
    return parseInt(this) === value;
  };

  assert.ok(arr.find(findNumber, 5) === 5, 'should find a valid element');
  assert.strictEqual(arr.find(findNumber, 11), undefined, 'undefined should be returned if the element can\'t be found');

  assert.throws(
    function() {
      // throw new TypeError('predicate must be a function');
      arr.find();
    },
    new TypeError('undefined is not a function'),
    'should throw an error if the predicate is not defined'
  );


  // should throw an error if the find is called on null isn't defined
  assert.throws(
    function() {
      Array.prototype.find.call(null, function() { return false; });
    },
    new TypeError('Array.prototype.find called on null or undefined'),
    'should throw an error if the find is called on null isn\'t defined'
  );
});
