// describe('array.findIndex polyfill', function() {
//   it('should be testing', function() {
//     expect(true).toEqual(true);
//   });

//   it('should be defined', function() {
//     expect(Array.prototype.findIndex).toBeDefined();
//   });

//   it('should throw an error if the array is not defined', function() {
//     expect(function() { 
//       Array.prototype.findIndex.call(null, function() { return false; });
//     }).toThrow(new TypeError('Array.prototype.findIndex called on null or undefined'));
//   });

//   it('should throw an error if the predicate is not defined', function() {
//     expect(function() { 
//       [1, 2, 4, 5, 6].findIndex();
//     }).toThrow(new TypeError('predicate must be a function'));
//   });

//   it('should return the correct item', function() {
//     var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

//     var find5 = function(value, index, list) {
//       return parseInt(this) === value;
//     };

//     expect(arr.findIndex(find5, 5)).toEqual(4);
//   });

//   it('should return undefined if the item can\'t be found', function() {
//     var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

//     var find5 = function(value, index, list) {
//       return parseInt(this) === value;
//     };

//     expect(arr.findIndex(find5, 11)).toEqual(-1);
//   });
// });

QUnit.test('array.findIndex', function(assert) {
  assert.ok(1 == "1", "Passed!");

  // it should be defined
  assert.ok(Array.prototype.findIndex !== undefined, 'findIndex is undefined');

  // should return the correct item
  var arr = [1,2,3,4,5,6,7,9,0];
  var find5 = function(value, index, list) {
    return parseInt(this) === value;
  };

  assert.ok(arr.findIndex(find5, 5) === 4, 'should find the elements index');
  
  // should return -1 if the item can't be found  
  assert.ok(arr.findIndex(find5, 100) === -1, 'should return -1 if element not found');

  // should throw an error if the predicate isn't defined
  assert.throws(
    function() {
      arr.findIndex();
    },
    new TypeError('undefined is not a function'),
    'should throw an error if the predicate is not defined'
  );

  // should throw an error if the findIndex is called on null isn't defined
  assert.throws(
    function() {
      Array.prototype.findIndex.call(null, function() { return false; });
    },
    new TypeError('Array.prototype.findIndex called on null or undefined'),
    'should throw an error if the findIndex is called on null isn\'t defined'
  );
});