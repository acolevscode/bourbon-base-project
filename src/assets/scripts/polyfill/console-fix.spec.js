// describe('console methods', function() {
//   it('should define each function', function() {
//     expect(console.assert).toBeDefined();
//     expect(console.clear).toBeDefined();
//     expect(console.count).toBeDefined();
//     expect(console.debug).toBeDefined();
//     expect(console.dir).toBeDefined();
//     expect(console.dirxml).toBeDefined();
//     expect(console.error).toBeDefined();
//     expect(console.exception).toBeDefined();
//     expect(console.group).toBeDefined();
//     expect(console.groupCollapsed).toBeDefined();
//     expect(console.groupEnd).toBeDefined();
//     expect(console.info).toBeDefined();
//     expect(console.log).toBeDefined();
//     expect(console.markTimeline).toBeDefined();
//     expect(console.profile).toBeDefined();
//     expect(console.profileEnd).toBeDefined();
//     expect(console.table).toBeDefined();
//     expect(console.time).toBeDefined();
//     expect(console.timeEnd).toBeDefined();
//     expect(console.timeline).toBeDefined();
//     expect(console.timelineEnd).toBeDefined();
//     expect(console.timeStamp).toBeDefined();
//     expect(console.trace).toBeDefined();
//     expect(console.warn).toBeDefined();
//     expect(console.test).toBeDefined();

//     expect(console.test()).toEqual(undefined);
//   });
// });

QUnit.test('array.findIndex', function(assert) {
  assert.ok(1 == "1", "Passed!");

  // it should be defined
  assert.ok(console.assert !== undefined, 'assert is undefined');
  assert.ok(console.clear !== undefined, 'clear is undefined');
  assert.ok(console.count !== undefined, 'count is undefined');
  assert.ok(console.debug !== undefined, 'debug is undefined');
  assert.ok(console.dir !== undefined, 'dir is undefined');
  assert.ok(console.dirxml !== undefined, 'dirxml is undefined');
  assert.ok(console.error !== undefined, 'error is undefined');
  assert.ok(console.exception !== undefined, 'exception is undefined');
  assert.ok(console.group !== undefined, 'group is undefined');
  assert.ok(console.groupCollapsed !== undefined, 'groupCollapsed is undefined');
  assert.ok(console.groupEnd !== undefined, 'groupEnd is undefined');
  assert.ok(console.info !== undefined, 'info is undefined');
  assert.ok(console.log !== undefined, 'log is undefined');
  assert.ok(console.markTimeline !== undefined, 'markTimeline is undefined');
  assert.ok(console.profile !== undefined, 'profile is undefined');
  assert.ok(console.profileEnd !== undefined, 'profileEnd is undefined');
  assert.ok(console.table !== undefined, 'table is undefined');
  assert.ok(console.time !== undefined, 'time is undefined');
  assert.ok(console.timeEnd !== undefined, 'timeEnd is undefined');
  assert.ok(console.timeline !== undefined, 'timeline is undefined');
  assert.ok(console.timelineEnd !== undefined, 'timelineEnd is undefined');
  assert.ok(console.timeStamp !== undefined, 'timeStamp is undefined');
  assert.ok(console.trace !== undefined, 'trace is undefined');
  assert.ok(console.warn !== undefined, 'warn is undefined');
  assert.ok(console.test() === undefined, 'test should return undefined');
});