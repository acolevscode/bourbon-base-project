// describe('Object.prototype.isEmpty', function() {
//   it('should return true', function() {
//     expect(''.isEmpty()).toEqual(true);
//   });

//   it('should return true', function() {
//     expect([].isEmpty()).toEqual(true);
//   });

//   it('should return true', function() {
//     expect({}.isEmpty()).toEqual(true);
//   });

//   it('should return true', function() {
//     expect({length: 0, custom_property: []}.isEmpty()).toEqual(true);
//   });

//   it('should return false', function() {
//     expect('Hello'.isEmpty()).toEqual(false);
//   });

//   it('should return false', function() {
//     expect([1, 2, 3].isEmpty()).toEqual(false);
//   });

//   it('should return false', function() {
//     expect({test: 1}.isEmpty()).toEqual(false);
//   });

//   it('should return false', function() {
//     expect({length: 1, custom_property: [1, 2, 3]}.isEmpty()).toEqual(false);
//   });
// });

QUnit.test('array.findIndex', function(assert) {
  assert.ok(1 == "1", "Passed!");

  // it should be defined
  assert.ok(Object.prototype.isEmpty !== undefined, 'findIndex is undefined');

  assert.ok(''.isEmpty() === true, '\'\' should be empty')
  assert.ok([].isEmpty() === true, '[] should be empty')
  assert.ok({}.isEmpty() === true, '{} should be empty')
  assert.ok({length: 0, custom_property: []}.isEmpty() === true, '{length: 0, custom_property: []} should be empty')
  assert.ok('Hello'.isEmpty() === false, '\'Hello\' should not be empty')
  assert.ok([1, 2, 3].isEmpty() === false, '[1, 2, 3] should not be empty')
  assert.ok({test: 1}.isEmpty() === false, '{test: 1} should not be empty')
  assert.ok({length: 1, custom_property: [1, 2, 3]}.isEmpty() === false, '{length: 1, custom_property: [1, 2, 3] should not be empty')
});