'use strict';

var comments = {
  'comments': [
    {
      'el': 'input',
      'title': 'Inputs',
      'comment': 'Most common form control, text-based input fields. Includes support for all HTML5 types: text, password, datetime, datetime-local, date, month, time, week, number, email, url, search, tel, and color.'
    },
    {
      'el': '.jumbotron',
      'title': 'Full Width Jumbotron',
      'comment': 'To make the jumbotron full width, and without rounded corners, place it outside all .containers and instead add a .container within.'
    },
    {
      'el': 'textarea',
      'title': 'Textarea',
      'comment': 'Form control which supports multiple lines of text. Change rows attribute as necessary.'
    },
    {
      'el': 'button,.btn,input[type=submit],input[type=button]',
      'title': 'Buttons',
      'comment': '<p>Buttons can have the following color classes.</p><ul><li>btn-danger</li><li>btn-success</li><li>btn-warning</li><li>btn-primary</li><li>btn-info</li></ul><p>Size classes</p><ul><li>btn-lg</li><li>btn-sm</li><li>btn-xs</li><li>btn-block</li><li>btn-block btn-full</li></ul>'
    },
    {
      'el': '.header',
      'title': 'Page header',
      'comment': '<p>The header block should be switched out based on the the user\'s context</p><p>For example, if the user is viewing the page through CSS they should see the Client Service System header.</p>'
    },
    {
      'el': '.site-footer',
      'title': 'Page footer',
      'comment': '<p>Currently this is a placeholder content. I\'m waiting on the decision to see if a footer is needed for these pages.</p><p>The footer can be split into multiple contexts similar to the page headers.</p>'
    },
    {
      'el': '.page-wrap',
      'title': 'Page Wrapper',
      'comment': '<p>The max width of the page can be set by adding the constrain class to the .page-wrap div.</p>'
    },
    {
      'el': '.site-header-dark,.site-footer-dark,.pre-footer-dark',
      'title': 'Dark Theme',
      'comment': '<p>The dark theme can be applied by adding the \'dark\' class to the body.</p>'
    }
  ]
};
