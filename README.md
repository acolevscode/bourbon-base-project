# Ameritas UI Starter Kit

## Overview

[Ameritas UI Starter Kit](http://ardock1d.ameritas.com:4040/skeleton/ui) is an opinionated boilerplate for web development. Tools for building a great experience across many devices and [performance oriented](#web-performance). 

### Features

| Feature                                | Summary                                                                                                                                                                                                                                                     |
|----------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Responsive boilerplate                 | A responsive boilerplate optimized for the multi-screen web.                                                                                                                                                                                                |
| Sass support                           | Compile [Sass](http://sass-lang.com/) into CSS with ease, bringing support for variables, mixins and more. (Run `grunt dev` or `grunt` for production)                                                                                                      |
| Performance optimization               | Minify and concatenate JavaScript, CSS, HTML and images to help keep your pages lean. (Run `grunt` to create an optimised version of your project to `/httpdocs`)                                                                                           |
| Code Linting                           | JavaScript code linting is done using [JSLint](http://jshint.com/) - a pluggable linter tool for identifying and reporting on patterns in JavaScript.                                                                                                       |
| Code Formatting                        | JavaScript code formatting is done using [JSCS](https://github.com/jscs-dev/grunt-jscs) - a pluggable tool for checking JavaScript Code Style with jscs.                                                                                                    |

## Quickstart

[Download](http://ardock1d.ameritas.com:4040/skeleton/ui/repository/archive.zip?ref=master) the kit or clone this repository and build on what is included in the `source` directory.

- `index.html` - no layout, but still includes our minimal mobile best-practices

## Development Process

Initialize the project

```sh
$ grunt init
```

### PatternLab

Build all assets, clean the source directory, and build the PatternLab

```sh
$ grunt patternlab
```

Update the PatternLab patterns and data

```sh
$ grunt patternlab.patterns
```

Update the PatternLab images

```sh
$ grunt patternlab.images
```

Update the PatternLab styles

```sh
$ grunt patternlab.styles
```



### Browser Support

At present, we officially aim to support the last two versions of the following browsers:

* Chrome
* Edge
* Firefox
* Safari
* Opera
* Internet Explorer 9+

### Docs and Recipes

* [Commands](http://ardock1d.ameritas.com:4040/skeleton/ui/blob/master/ui/docs/commands.md) - What are the grunt commands?
* [HTML](http://ardock1d.ameritas.com:4040/skeleton/ui/blob/master/ui/docs/html.md) - How HTML files are processed.
* [Stylesheets](http://ardock1d.ameritas.com:4040/skeleton/ui/blob/master/ui/docs/styles.md) - How Stylesheets are processed.
* [JavaScript](http://ardock1d.ameritas.com:4040/skeleton/ui/blob/master/ui/docs/javascript.md) - How JavaScript files are processed.
* [Images](http://ardock1d.ameritas.com:4040/skeleton/ui/blob/master/ui/docs/images.md) - How images are processed.
* [Pattern Libraries](http://ardock1d.ameritas.com:4040/skeleton/ui/blob/master/ui/docs/patterns.md) - How images are processed.
* [File Appendix](http://ardock1d.ameritas.com:4040/skeleton/ui/tree/master/docs/file-appendix.md) - What do the different files here do?

## Openshift Project setup

Fork the repository that you want to use. If you are using a repo that is not hosted on the ardock1d server, you will need to create a BuildConfig that includes the Ameritas proxy server information.

Login to the [Web Console](https://ardock1d.ameritas.com:8443/) for Openshift.

Create a new project by hitting the New Project button. Enter the project name, display name, and project description.

Select the php template under Instant Apps, and enter the parameters for the application.

Be sure to clean up the Docker registry.
- ssh into ardock1d.ameritas.com
- as root run
  - docker rm $(docker ps -q -f status=exited)
