module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',

    config: 'config',

    src: 'src',
    tmp: '.tmp',
    pubs: 'docs',

    site: 'dist',
    javascript: '<%= tmp %>/assets/scripts',
    libs: 'vendor',
    patternlab: 'patternlab',

    reporting: '<%= pubs %>',
    platoPath: '<%= reporting %>/plato',
    karmaCoverageReport: '<%= reporting %>/coverage/'
  });

  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('default', [
    'clean',
    'copy:generic',
    'styles',
    'images',
    'html',
    'js',
    'shell:patternlab',
    'sassdoc'
  ]);
  // grunt.registerTask('dev', ['default', 'watch']);
  grunt.registerTask('styleguides', ['sassdoc', 'patternlab']);

  // process html files
  grunt.registerTask('html', ['processhtml:dist', 'htmlmin']);
  grunt.registerTask('html.dev', ['processhtml:dev', 'htmlmin']);

  // process js files
  grunt.registerTask('js', [
    'concurrent:jsStandards',
    'uglify:dist',
    'copy:js',
    'karma:unit'
  ]);

  // process images
  grunt.registerTask('images', ['newer:imagemin:dynamic', 'copy:images']);

  // process styles
  grunt.registerTask('styles', ['sass:dist', 'copy:css']);

  // build the site
  grunt.registerTask('site', [
    'clean:site',
    'concurrent:site'
  ]);

  grunt.registerTask('site.sync', [
    'site',
    'browserSync:site',
    'watch'
  ]);

  grunt.registerTask('patternlab', [
    'clean:patternlab',
    'concurrent:patternlab',
    'shell:patternlab'
  ]);

  grunt.registerTask('patternlab.sync', [
    'patternlab',
    'browserSync:patternlab',
    'watch'
  ]);

  grunt.loadTasks(grunt.config.get('config') + '/grunt');
};