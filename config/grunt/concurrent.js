module.exports = function(grunt) {
  grunt.config('concurrent', {
    jsStandards: ['jshint', 'jscs', 'plato'],
    build: ['copy:generic', 'js', 'images', 'html', 'styles'],
    clean: ['clean:tmp', 'clean:patternlab', 'clean:site'],
    site: [
      'copy:generic',
      'styles',
      'images',
      'html',
      'js'
    ],
    patternlab: [
      'copy:generic',
      'styles',
      'images',
      'js'
    ]
  });

  grunt.loadNpmTasks('grunt-concurrent');
};
