module.exports = function(grunt) {
  grunt.config('copy', {
    init: {
      files: [{
        expand: true,
        cwd: 'patternlab/core/styleguide/',
        src: ['**'],
        dest: '<%= patternlab %>/public/styleguide/'
      }]
    },
    images: {
      files: [{
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: ['**/*.{png,jpg,gif,svg}'], 
        dest: '<%= site %>/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: ['**/*.{png,jpg,gif,svg}'], 
        dest: '<%= patternlab %>/source/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: ['**/*.{png,jpg,gif,svg}'], 
        dest: '<%= patternlab %>/public/', 
        filter: 'isFile'
      }]
    },
    css: {
      files: [{
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: [
          '**/*.css',
          '**/*.css.map',
          '!**/pattern-lab-styles*.*'
        ], 
        dest: '<%= site %>/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: [
          '**/*.css',
          '**/*.css.map'
        ], 
        dest: '<%= patternlab %>/source/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: [
          '**/*.css',
          '**/*.css.map'
        ], 
        dest: '<%= patternlab %>/public/', 
        filter: 'isFile'
      }]
    },
    js: {
      files: [{
        expand: true,
        cwd: '<%= libs %>',
        src: grunt.file.readJSON('config/javascript.json').static,
        dest: '<%= patternlab %>/source/assets/scripts/libraries/'
      }, {
        expand: true,
        cwd: '<%= libs %>',
        src: grunt.file.readJSON('config/javascript.json').static,
        dest: '<%= javascript %>/libraries/'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: [
          '**/*.js',
          '**/*.js.map'
        ], 
        dest: '<%= site %>/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: [
          '**/*.js',
          '**/*.js.map'
        ], 
        dest: '<%= patternlab %>/source/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= tmp %>/', 
        src: [
          '**/*.js',
          '**/*.js.map'
        ], 
        dest: '<%= patternlab %>/public/', 
        filter: 'isFile'
      }]
    },
    generic: {
      files: [{
        expand: true, 
        cwd: '<%= src %>/', 
        src: [
          '**',
          '!_data/**',
          '!_patterns/**',
          '!scss/**',
          '!**/*.{js,png,jpg,gif,svg,html,scss}'
        ], 
        dest: '<%= site %>/', 
        filter: 'isFile'
      }, {
        expand: true, 
        cwd: '<%= src %>/', 
        src: [
          '**',
          '!scss/**',
          '!**/*.{js,png,jpg,gif,svg,html,scss}'
        ], 
        dest: '<%= patternlab %>/source/', 
        filter: 'isFile'
      }]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
};
