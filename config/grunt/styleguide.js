module.exports = function(grunt) {
  // https://github.com/jharding/grunt-exec
  // grunt.config('exec', {
  //   patternlab: 'php <%= patternlab %>/core/builder.php -g'
  //   // patternlab: 'php httpdocs/patternlab/core/builder.php -g'
  // });

  grunt.config('shell', {
    options: {
      stderr: true
    },
    patternlab: {
      command: 'php <%= patternlab %>/core/builder.php -g'
    }
  });

  grunt.config('sassdoc', {
    default: {
      src: '<%= src %>/assets/scss/**/*.scss',
      options: {
        config: '<%= config %>/sassdocs.json'
      }
    }
  });

  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-sassdoc');
};
