module.exports = function(grunt) {
  grunt.config('sass', {
    options: {
      sourceMap: true,
      precision: 5,
      sourceComments: false,
      sourceMapContents: true,
      // sourceMapEmbed: true,
      outputStyle: 'compressed',  // nested, expanded, compact, compressed
    },
    dist: {
      files: {
        '<%= tmp %>/assets/css/styles.css': '<%= src %>/assets/scss/styles.scss'
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
};
