module.exports = function(grunt) {
  // validate Javascript files with JSHint
  grunt.config('jshint', {
    options: {
      jshintrc: '<%= config %>/jshintrc',
      reporter: require('jshint-stylish')
    },
    app: [
      '<%= src %>/**/*.js',
      '!<%= src %>/**/*.spec.js'
    ]
  });

  // check the JavaScritp Code Style with jscs
  grunt.config('jscs', {
    src: [
      '<%= src %>/**/*.js',
      '!<%= src %>/**/*.spec.js'
    ],
    options: {
      config: '<%= config %>/jscsrc', // https://google.github.io/styleguide/javascriptguide.xml
      esnext: false, // If you use ES6 http://jscs.info/overview.html#esnext 
      verbose: true, // If you need output with rule names http://jscs.info/overview.html#verbose 
      fix: false // Autofix code style violations when possible. 
    }
  });

  // generate a complexity analysis report with plato
  grunt.config('plato', {
    options: {
      jshint: grunt.file.readJSON(grunt.config.get('config') + '/jshintrc')
    },
    app: {
      files: {
        '<%= platoPath %>': [
          '<%= src %>/**/*.js',
          '!<%= src %>/**/*.spec.js',
          '!<%= src %>/scss/**',
          '!<%= src %>/_data/**',
          '!<%= src %>/_patterns/**'
        ]
      }
    }
  });

  grunt.config('karma', {
    options: {
      configFile: '<%= config %>/karma.conf.js',
      basePath: '../',
      files: grunt.file.readJSON(grunt.config.get('config') + '/javascript.json').vendors.concat([
        '<%= src %>/**/*.js'
      ]),
      coverageReporter: {
        dir: '<%= karmaCoverageReport %>',
        subdir: '.',
        check: {
          global: {
            statements: [50, 75],
            branches: [50, 75],
            functions: [50, 75],
            lines: [50, 75]
          },
          each: {
            statements: [50, 75],
            branches: [50, 75],
            functions: [50, 75],
            lines: [50, 75]
          }
        }
      }
    },
    unit: {
      options: {
        singleRun: true
      }
    },
    dev: {
      options: {
        singleRun: false
      }
    }
  });

  // concatenate and compress all of the javascript files
  grunt.config('uglify', {
    options: {
      mangle: true,
      compress: true,
      beautify: false,
      sourceMap: true,
      sourceMapIncludeSources: true,
      preserveComments: false
      // banner: '<%= banner %>'
    },
    dist: {
      files: {
        '<%= javascript %>/script.min.js': grunt.file.readJSON(grunt.config.get('config') + '/javascript.json').vendors.concat([
          '<%= src %>/**/*.js',
          '!<%= src %>/**/*.spec.js'
        ])
      }
    },
    test: {
      files: {
        '<%= javascript %>/base.js': [
          '<%= src %>/**/*.js',
          '!<%= src %>/**/*.spec.js'
        ],
        '<%= javascript %>/base.spec.js': [
          '<%= src %>/**/*.spec.js'
        ]
      }
    },
    all: {
      files: {
        '<%= javascript %>/script.min.js': grunt.file.readJSON(grunt.config.get('config') + '/javascript.json').vendors.concat([
          '<%= src %>/**/*.js',
          '!<%= src %>/**/*.spec.js'
        ]),
        '<%= javascript %>/base.js': [
          '<%= src %>/**/*.js',
          '!<%= src %>/**/*.spec.js'
        ],
        '<%= javascript %>/base.spec.js': [
          '<%= src %>/**/*.spec.js'
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-jscs');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-plato');
};
