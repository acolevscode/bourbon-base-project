module.exports = function(grunt) {

  grunt.config('watch', {
    scripts: {
      files: ['<%= src %>/**/*.js'],
      tasks: ['js']
    },
    images: {
      files: ['<%= src %>/**/*.{png,jpg,gif,svg}'],
      tasks: ['images']
    },
    html: {
      files: ['<%= src %>/**/*.html'],
      tasks: ['html']
    },
    styles: {
      files: ['<%= src %>/**/*.{scss,css}'],
      tasks: ['styles']
    },
    generic: {
      files: [
        '<%= src %>/**',
        '!<%= src %>/**/*.{js,png,jpg,gif,svg,html,scss,css}'
      ],
      tasks: ['copy:generic']
    },
    mustache: {
      files: [
        '<%= src %>/**/*.mustache'
      ],
      tasks: ['copy:patternlab', 'exec:patternlab']
    }
  });


  grunt.config('browserSync', {
    site: {
      options: {
        watchTask: true,
        server: {
          baseDir: "<%= site %>/"
        }
      },
      src : '<%= site %>/**/*.{css,html,js}'
    },
    patternlab: {
      options: {
        watchTask: true,
        server: {
          baseDir: '<%= patternlab %>/public/'
        }
      },
      src : '<%= patternlab %>/public/**/*.{css,html,js}'
    }
  });


  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-watch');
};
