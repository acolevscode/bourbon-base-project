module.exports = function(grunt) {
  // optimize images
  grunt.config('htmlmin', {
    dist: {
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      expand: true,
      cwd: '<%= site %>/',
      src: ['**/*.html'],
      dest: '<%= site %>/'
    }
  });

  grunt.config('processhtml', {
    dev: {
      files: [{
        expand: true,
        cwd: '<%= src %>/',
        src: ['*.html'],
        dest: '<%= site %>/',
        ext: '.html'
      }]
    },
    dist: {
      files: [{
        expand: true,
        cwd: '<%= src %>/',
        src: ['*.html'],
        dest: '<%= site %>/',
        ext: '.html'
      }]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-processhtml');
};
