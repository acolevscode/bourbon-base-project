module.exports = function(grunt) {
  // optimize images
  grunt.config('imagemin', {
    dynamic: {
      files: [{
        expand: true,
        cwd: '<%= src %>/',
        src: ['**/*.{png,jpg,gif,svg}'],
        dest: '<%= tmp %>/'
      }]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-imagemin');
};
