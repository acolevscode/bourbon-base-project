module.exports = function(grunt) {
  grunt.config('clean', {
    tmp: ['<%= tmp %>/'],
    patternlab: ['<%= patternlab %>/source/'],
    site: ['<%= site %>'],
    init: [
      '<%= tmp %>/',
      '<%= patternlab %>/source/',
      '<%= site %>/',
      'node_modules/',
      '<%= patternlab %>/public/'
    ]
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
};
